/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/12 13:36:34 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/13 11:30:54 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

int		count_total_size(int size, char **strs, char *sep)
{
	const int	sep_size = ft_strlen(sep);
	const int	final_size = size;
	int			total_size;

	total_size = 0;
	while (size--)
		total_size += ft_strlen(*strs++);
	total_size += (sep_size * (final_size - 1));
	return (total_size);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	const int	total_size = (size > 0) ? count_total_size(size, strs, sep) : 0;
	const char	*sep_begin = sep;
	char		*begin;
	char		*concat;
	int			index;

	index = 0;
	concat = (char*)malloc(sizeof(char) * total_size + 1);
	if (concat == NULL)
		return (NULL);
	begin = concat;
	while (size > 0)
	{
		while (*strs[index])
			*concat++ = *strs[index]++;
		index++;
		if (size != 1)
			while (*sep)
				*concat++ = *sep++;
		sep = (char*)sep_begin;
		size--;
	}
	*concat = '\0';
	return (begin);
}
