/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/12 13:12:19 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/12 13:30:40 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	const int	delta = max - min;
	int			*ret;

	if (delta <= 0)
	{
		*range = NULL;
		return (0);
	}
	ret = (int*)malloc(sizeof(int) * delta);
	if (ret == NULL)
		return (-1);
	while (min < max)
		*ret++ = min++;
	*range = ret - delta;
	return (delta);
}
