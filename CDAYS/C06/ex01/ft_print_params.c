/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 15:02:32 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/11 15:43:19 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	write(1, final_str, str - final_str);
}

int		main(int argc, char **argv)
{
	unsigned int index;

	index = 1;
	while (argc-- > 1)
	{
		ft_putstr(argv[index++]);
		write(1, "\n", 1);
	}
	return (0);
}
