/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 15:47:42 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/12 08:31:04 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr(char *str)
{
	const char	*final_str = str;

	while (*str)
		str++;
	write(1, final_str, str - final_str);
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1 && *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}

int		main(int argc, char **argv)
{
	int		index;
	char	*tmp;

	index = 1;
	while (index < argc - 1)
	{
		if (ft_strcmp(argv[index], argv[index + 1]) > 0)
		{
			tmp = argv[index];
			argv[index] = argv[index + 1];
			argv[index + 1] = tmp;
			index = 1;
		}
		else
			index++;
	}
	index = 1;
	while (index < argc)
	{
		ft_putstr(argv[index++]);
		ft_putstr("\n");
	}
	return (0);
}
