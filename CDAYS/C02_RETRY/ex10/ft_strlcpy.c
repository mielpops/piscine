/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlcpy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 11:54:00 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 09:51:24 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char			*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const char *final_dest = dest;

	while (n--)
		*dest++ = (*src) ? *src++ : '\0';
	*dest = '\0';
	return ((char*)final_dest);
}

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	const unsigned int src_len = ft_strlen(src);

	if (src_len + 1 < size)
		ft_strncpy(dest, src, src_len + 1);
	else if (size != 0)
	{
		ft_strncpy(dest, src, size - 1);
		dest[size - 1] = '\0';
	}
	return (src_len);
}
