/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 13:44:45 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/06 22:07:21 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const char *final_dest = dest;

	while (n--)
		*dest++ = (*src) ? *src++ : '\0';
	*dest = '\0';
	return ((char*)final_dest);
}
