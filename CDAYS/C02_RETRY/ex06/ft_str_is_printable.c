/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 20:10:46 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/04 22:14:49 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_printable(char c)
{
	return ((c >= 32 && c <= 127) ? 1 : 0);
}

int		ft_str_is_printable(char *str)
{
	while (*str)
	{
		if (!is_printable(*str++))
			return (0);
	}
	return (1);
}
