/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 16:11:34 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/24 14:59:55 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_in(int c, char *charset)
{
	while (*charset)
	{
		if (*charset == (unsigned char)c)
			return (1);
		charset++;
	}
	return (0);
}

int		ft_count_words(char *str, char *charset)
{
	int			counter;

	if (*charset == '\0')
		return (0);
	counter = 0;
	while (*str)
	{
		if (!ft_in(*str, charset))
		{
			counter++;
			while (!ft_in(*str, charset))
				str++;
		}
		else
			str++;
	}
	return (counter);
}

int		ft_strlen_sep(char *str, char *charset)
{
	const char *final_str = str;

	while (!ft_in(*str, charset) && *str)
		str++;
	return (str - final_str);
}

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const char *final_dest = dest;

	while (n--)
		*dest++ = (*src) ? *src++ : '\0';
	return ((char*)final_dest);
}

char	**ft_split(char *str, char *charset)
{
	const int	words = ft_count_words(str, charset);
	int			cpy_size;
	char		**arr;
	int			i;

	if (!(arr = malloc(sizeof(char*) * (words + 1))) || !*str)
		return (NULL);
	i = 0;
	while (*str)
	{
		if (!ft_in(*str, charset))
		{
			cpy_size = ft_strlen_sep(str, charset);
			if ((arr[i] = malloc(sizeof(char) * (cpy_size + 1))) == NULL)
				return (NULL);
			ft_strncpy(arr[i], str, cpy_size);
			arr[i++][cpy_size] = '\0';
			while (*str && !ft_in(*str, charset))
				str++;
		}
		else
			str++;
	}
	arr[words] = NULL;
	return (arr);
}

int		main(int argc, char **argv)
{
	if (argc > 2)
	{
		char **array = ft_split(argv[1], argv[2]);

		while (*array != NULL)
			printf("%s\n", *array++);
	}
}
