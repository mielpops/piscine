/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 19:35:51 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/04 19:51:47 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_uppercase(char c)
{
	return ((c >= 'A' && c <= 'Z') ? 1 : 0);
}

int		ft_str_is_uppercase(char *str)
{
	while (*str)
	{
		if (!is_uppercase(*str++))
			return (0);
	}
	return (1);
}
