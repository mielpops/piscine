/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 21:20:32 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/17 13:00:02 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_ABS_H
#	define FT_ABS_H

#	define ABS(Value) ((Value < 0) ? -Value : Value)

#	endif
