/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do-op.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 22:33:39 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/22 22:34:17 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_DO_OP_H
#	define FT_DO_OP_H

#	define DIV_ERROR "Stop : division by zero"
#	define MOD_ERROR "Stop : modulo by zero"

#include <unistd.h>

enum	e_error_flags {
	MOD_FLAG = 1,
	DIV_FLAG = 2
};

enum	e_router {
	SUM,
	SUB,
	MULT,
	DIV,
	MOD
};

void	ft_putstr(char *str);
void	ft_putchar(char c);
void	ft_putnbr(int nbr);
int		ft_atoi(char *str);
int		ft_mult(int a, int b);
int		ft_sum(int a, int b);
int		ft_sub(int a, int b);
int		ft_div(int a, int b);
int		ft_mod(int a, int b);

#	endif
