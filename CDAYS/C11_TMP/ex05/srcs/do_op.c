/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do-op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 17:21:37 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/24 15:29:26 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <do_op.h>

int		is_operator(char op)
{
	return ((op == '+' || op == '-' || op == '/' || op == '*'
				|| op == '%') ? 1 : 0);
}

void	print_error(int flag)
{
	if (flag == MOD_FLAG)
		ft_putstr(MOD_ERROR);
	if (flag == DIV_FLAG)
		ft_putstr(DIV_ERROR);
}

void	do_op(int op_1, int op_2, char operator, int (*operations[])(int, int))
{
	if (is_operator(operator))
	{
		if (operator == '+')
			ft_putnbr(operations[SUM](op_1, op_2));
		if (operator == '-')
			ft_putnbr(operations[SUB](op_1, op_2));
		if (operator == '*')
			ft_putnbr(operations[MULT](op_1, op_2));
		if (operator == '/')
		{
			if (op_2 != 0)
				ft_putnbr(operations[DIV](op_1, op_2));
			else
				print_error(DIV_FLAG);
		}
		if (operator == '%')
		{
			if (op_2 != 0)
				ft_putnbr(operations[MOD](op_1, op_2));
			else
				print_error(MOD_FLAG);
		}
	}
	else
		ft_putchar('0');
}

int		main(int argc, char **argv)
{
	int(*operations[5])(int, int);

	if (argc > 3)
	{
		operations[0] = &ft_sum;
		operations[1] = &ft_sub;
		operations[2] = &ft_mult;
		operations[3] = &ft_div;
		operations[4] = &ft_mod;
		if (argv[2][1] == '\0')
			do_op(ft_atoi(argv[1]), ft_atoi(argv[3]), *argv[2], operations);
		else
			ft_putchar('0');
		ft_putchar('\n');
	}
	return (0);
}
