/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_string_tab.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 17:46:31 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/24 18:04:35 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_ptr_swap(char **p1, char **p2)
{
	char *tmp;

	tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

int		ft_tab_len(char **tab)
{
	int counter;

	counter = 0;
	while (*tab != NULL)
	{
		counter++;
		tab++;
	}
	return (counter);
}

void	ft_advanced_sort_string_tab(char **tab, int (*cmp)(char*, char*))
{
	const int	size = ft_tab_len(tab);
	int			index;
	int			tab_size;

	tab_size = size;
	index = 0;
	while (tab_size-- > 1)
	{
		if (cmp(tab[index], tab[index + 1]) > 0)
		{
			ft_ptr_swap(&tab[index], &tab[index + 1]);
			index = 0;
			tab_size = size;
		}
		else
			index++;
	}
}

int		ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1 && *s2)
	{
		s1++;
		s2++;
	}
	return (*s1 - *s2);
}
