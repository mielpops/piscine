/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:48:15 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 15:40:52 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	const char	*final_to_find = to_find;
	int			offset;

	offset = 0;
	while (*str)
	{
		while (*str == *to_find && *str && *to_find)
		{
			str++;
			to_find++;
			offset++;
		}
		if (*to_find == '\0')
			return (str - offset);
		if (*str == '\0')
			return (str);
		to_find = (char*)final_to_find;
		offset = 0;
		str++;
	}
	return (str);
}
