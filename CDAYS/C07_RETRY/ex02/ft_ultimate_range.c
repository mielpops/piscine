/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/12 13:12:19 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/20 15:10:54 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_ultimate_range(int **range, int min, int max)
{
	const int	final_min = min;
	int			*ret;
	int			index;

	*range = NULL;
	if (min >= max)
		return (0);
	ret = (int*)malloc(sizeof(int) * (max - min));
	if (ret == NULL)
		return (-1);
	index = 0;
	while (min < max)
		ret[index++] = min++;
	*range = ret;
	return (max - final_min);
}
