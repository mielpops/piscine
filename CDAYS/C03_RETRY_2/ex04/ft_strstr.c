/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:48:15 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/10 21:47:11 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	const char	*needle = to_find;
	char		*begin;

	if (*to_find == '\0')
		return (str);
	while (*str)
	{
		begin = str;
		if (*str == *to_find)
		{
			while (*str == *to_find && *str && *to_find)
			{
				to_find++;
				str++;
			}
			if (*to_find == '\0')
				return (begin);
			if (*str == '\0')
				return (0);
		}
		else
			str++;
		to_find = (char*)needle;
	}
	return (0);
}
