/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 07:55:14 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/04 12:42:58 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	ft_rev_int_tab(int *tab, int size)
{
	int begin;
	int pivot;

	pivot = size / 2;
	pivot = (pivot % 2 == 1) ? pivot : pivot - 1;
	begin = 0;
	while (begin <= pivot)
		swap(&tab[begin++], &tab[--size]);
}
