/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:27:10 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/09 17:21:05 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	const int dest_len = ft_strlen(dest);
	const int final_n = nb;

	if (nb == 0)
		return (dest);
	dest = dest + dest_len;
	while (nb--)
		*dest++ = *src++;
	*dest = '\0';
	return (dest - (final_n + dest_len));
}
