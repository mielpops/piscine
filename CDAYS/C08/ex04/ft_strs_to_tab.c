/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strs_to_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 21:44:54 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/22 13:40:00 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_str.h"
#include <stdlib.h>

int					ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char				*ft_strdup(char *str)
{
	const char	*final_str = str;
	char		*begin;
	char		*duplicate;

	while (*str)
		str++;
	duplicate = (char*)malloc(sizeof(char) * (str - final_str + 1));
	if (duplicate == NULL)
		return (NULL);
	begin = duplicate;
	str = (char*)final_str;
	while (*str)
		*duplicate++ = *str++;
	*duplicate = '\0';
	return (begin);
}

struct s_stock_str	*ft_strs_to_tab(int ac, char **av)
{
	struct s_stock_str	*s_tab;
	int					index;
	int					size;

	s_tab = malloc(sizeof(t_stock_str) * (ac + 1));
	if (s_tab == NULL)
		return (NULL);
	index = 0;
	while (index < ac)
	{
		size = ft_strlen(av[index]);
		s_tab[index].size = size;
		s_tab[index].str = av[index];
		s_tab[index].copy = ft_strdup(av[index]);
		if (s_tab[index].copy == NULL)
			return (NULL);
		index++;
	}
	s_tab[index].str = 0;
	return (s_tab);
}
