/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 16:09:01 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/03 18:24:49 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	init_variables(char *units, char *tens, char *hundreds)
{
	*units = '0';
	*tens = '1';
	*hundreds = '2';
}

void	format_numbers(char units, char tens, char hundreds)
{
	write(1, &units, 1);
	write(1, &tens, 1);
	write(1, &hundreds, 1);
	if (units != '7')
		write(1, ", ", 2);
}

void	ft_print_comb(void)
{
	char units;
	char hundreds;
	char tens;

	init_variables(&units, &tens, &hundreds);
	while (units <= '7')
	{
		while (tens <= '8')
		{
			while (hundreds <= '9')
			{
				if (units < tens && tens < hundreds && units < hundreds)
					format_numbers(units, tens, hundreds);
				hundreds++;
			}
			tens++;
			hundreds = 0;
		}
		units++;
		tens = 0;
	}
}
