/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 08:23:24 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/06 17:11:14 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_alpha(char c)
{
	return (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) ? 1 : 0);
}

int		is_uppercase(char c)
{
	return ((c >= 'A' && c <= 'Z') ? 1 : 0);
}

char	*ft_strlowcase(char *str)
{
	const char *final_str = str;

	while (*str)
	{
		if (is_alpha(*str) && is_uppercase(*str))
			*str = *str + 32;
		str++;
	}
	return ((char*)final_str);
}

char	*ft_strcapitalize(char *str)
{
	const char	*final_str = str;
	int			is_begin;

	str = ft_strlowcase(str);
	while (*str)
	{
		is_begin = ((final_str - str) == 0 && is_alpha(*str)) ? 1 : 0;
		if (is_begin || (!is_alpha(*(str - 1)) && is_alpha(*str)))
			*str = *str - 32;
		str++;
	}
	return ((char*)final_str);
}
