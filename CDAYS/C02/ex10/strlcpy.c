/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlcpy.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 11:54:00 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/05 14:07:30 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	const int src_len = ft_strlen(src);

	while (--size > 0)
		*dest++ = *src++;
	*dest = '\0';
	return (src_len);
}
