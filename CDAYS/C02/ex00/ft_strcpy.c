/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 12:54:23 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/06 17:08:00 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *dest, char *src)
{
	const char *final_dest = dest;

	while (*src)
		*dest++ = *src++;
	*dest = *src;
	return ((char*)final_dest);
}
