/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlib.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/13 15:24:53 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/13 17:51:15 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_is_space(char c)
{
	return ((c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r' ||
				c == ' ') ? 1 : 0);
}

int		ft_is_number(char c)
{
	return ((c >= '0' && c <= '9') ? 1 : 0);
}

int		ft_count_words(char *str)
{
	int counter;

	counter = 0;
	while (*str)
	{
		if (!ft_is_space(*str))
		{
			counter++;
			while (!ft_is_space(*str) && *str)
				str++;
		}
		else
			str++;
	}
	return (counter);
}

int		ft_strlen_whitespaces(char *str)
{
	const char *final_str = str;

	while (!ft_is_space(*str) && *str)
		str++;
	return (str - final_str);
}

int		*ft_str_to_int_vector(char *str)
{
	const int	size = ft_count_words(str);
	int			*vector;
	int			*begin;

	vector = (int*)malloc(sizeof(int*) * (size + 1));
	if (vector == NULL)
		return (NULL);
	begin = vector;
	while (*str)
	{
		if (ft_is_number(*str))
		{
			if (ft_strlen_whitespaces(str) == 1)
				*vector++ = *str - '0';
			else
				return (0);
		}
		str++;
	}
	vector[size] = 0;
	return (begin);
}
