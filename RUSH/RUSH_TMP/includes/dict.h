/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jcueille <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 18:06:07 by jcueille          #+#    #+#             */
/*   Updated: 2019/07/21 15:08:25 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_DICT_H
#	define FT_DICT_H

#	define DICT_SEPARATOR ":"

typedef struct	s_dict
{
	char *key;
	char *value;
}				t_dict;

#	endif