/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 22:58:43 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/19 11:42:50 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *str)
{
	const char	*final_str = str;
	char		*begin;
	char		*duplicate;

	while (*str)
		str++;
	duplicate = (char*)malloc(sizeof(char) * (str - final_str + 1));
	if (duplicate == NULL)
		return (NULL);
	begin = duplicate;
	str = (char*)final_str;
	while (*str)
		*duplicate++ = *str++;
	*duplicate = '\0';
	return (begin);
}
