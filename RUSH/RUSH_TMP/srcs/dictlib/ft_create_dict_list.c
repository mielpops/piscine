/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_dict_list.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 21:56:21 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 21:56:47 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dict.h>
#include <rush.h>

t_dict  *ft_create_dict_list(int size)
{
    t_dict *dict;

    dict = malloc(sizeof(t_dict) * size);
    if (dict == NULL)
        return (NULL);
    return (dict);
}
