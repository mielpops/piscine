/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 10:35:47 by lperson-          #+#    #+#             */
/*   Updated: 2019/07/23 11:21:55 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IO_H

# define IO_H

# define EOF	(-1)

enum	e_out
{
	STD_IN,
	STD_OUT,
	STD_ERR
};

int		ft_getchar_fd(int fd);
void	ft_putstr_fd(char *str, int fd);

#endif
