/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   io.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 10:30:28 by lperson-          #+#    #+#             */
/*   Updated: 2019/07/23 11:22:28 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "io.h"

int		ft_getchar_fd(int fd)
{
	int		c;

	c = read(fd, &c, 1);
	if (c == 0)
		return (EOF);
	else if (c == -1)
		return (0);
	return (c);
}

void	ft_putstr_fd(char *str, int fd)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	write(fd, str, i);
}
