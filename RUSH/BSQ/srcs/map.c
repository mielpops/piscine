/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 11:55:48 by lperson-          #+#    #+#             */
/*   Updated: 2019/07/23 17:28:36 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"
#include "strings.h"
#include <stdlib.h>

t_map			ft_voidmap(void)
{
	t_map	map;

	map.len_x = 0;
	map.len_y = 0;
	map.obstacles = NULL;
	return (map);
}

long			ft_countobs(char **map, char obstacle)
{
	int		i;
	int		d;
	long	count;

	i = 0;
	count = 0;
	while (map[i])
	{
		d = 0;
		while (map[i][d])
			if (map[i][d++] == obstacle)
				count++;
		i++;
	}
	return (count);
}

t_coordinate	*ft_getobstacles(char **map, char obstacle)
{
	int				i;
	int				d;
	long			size;
	t_coordinate	*obs;

	i = 0;
	size = ft_countobs(map, obstacle);
	if (!(obs = (t_coordinate*)malloc(sizeof(t_coordinate) * (size + 1))))
		return (NULL);
	while (map[i])
	{
		d = 0;
		while (map[i][d])
			if (map[i][d++] == obstacle)
			{
				obs->x = d - 1;
				obs->y = i;
				++obs;
			}
		i++;
	}
	obs->x = -1;
	obs->y = -1;
	return (obs - size);
}

int			ft_checkmap(char **map)
{
	int		size;

	size = ft_strlen(*map);
	while (*map)
	{
		if (size != ft_strlen(*map))
			return (-1);
		map++;
	}
	return (1);
}
