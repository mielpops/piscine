/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/06 17:41:11 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 10:26:31 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rush.h>
#include <dict.h>

char	*ft_strcpy(char *dest, char *src)
{
	const char *final_dest = dest;

	while (*src)
		*dest++ = *src++;
	*dest = *src;
	return ((char*)final_dest);
}

char	*ft_strcat(char *dest, char *src)
{
	const int dest_len = ft_strlen(dest);
	const int src_len = ft_strlen(src);

	ft_strcpy(dest + dest_len, src);
	dest[dest_len + src_len] = '\0';
	return (dest);
}
