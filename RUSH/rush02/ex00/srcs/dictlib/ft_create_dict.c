/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_dict.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 21:57:41 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 21:58:26 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <dict.h>
#include <rush.h>

t_dict  *ft_create_dict(char *key, char *value)
{
    t_dict *dict;

    dict = malloc(sizeof(t_dict*));
    if (dict == NULL)    
        return (NULL);
    dict->key = ft_strdup(key);
    dict->value = ft_strdup(value);
    if (dict->key == NULL || dict->value == NULL)
        return (NULL);
    return (dict);
}