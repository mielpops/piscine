#include <stdio.h>

int        main(int argc, char **argv)
{
	if (argc > 2)
	{
		int min = atoi(argv[1]);
		int max = atoi(argv[2]);

		int *array = ft_range(min, max);
		if (array != NULL)
			for (int i = 0 ; i < max - min ; i++)
				printf("[%i]", array[i]);
	}
}
