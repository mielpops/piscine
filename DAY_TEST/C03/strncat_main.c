#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 10

int        main(int argc, char **argv)
{
	if (argc > 2)
	{
		char buffer_1[SIZE] = "hello";
		char buffer_2[SIZE] = "hello";

		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_1[i]);

		printf("\n");
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_2[i]);

		printf("\n");


		printf("Before :\nOriginal : %s | my : %s\n", buffer_1, buffer_2);

		strncat(buffer_1, argv[1], atoi(argv[2]));
		ft_strncat(buffer_2, argv[1], atoi(argv[2]));
		printf("After :\nOriginal : %s | my : %s\n", buffer_1, buffer_2);
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_1[i]);

		printf("\n");
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_2[i]);

		printf("\n");

	}
	return (0);
}
