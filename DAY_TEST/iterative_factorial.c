/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iterative_factorial.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 10:56:03 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/10 10:56:51 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int        main(int argc, char **argv)
{
	if (argc > 1)
		printf("The value is : %i\n", ft_iterative_factorial(atoi(argv[1])));
	return (0);
}
