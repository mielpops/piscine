/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   STRLCPY.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 13:37:13 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/05 13:38:12 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int        main(int argc, char **argv)
{
	char buffer[SIZE];
	char buffer2[SIZE];

	if (argc > 1)
	{


		printf("\n MY STRLCPY \n");
		for (int i = 0 ; i < SIZE ; i++)
			printf("[%d] ", buffer[i]);

		ft_strlcpy(buffer, argv[1], SIZE - 1);
		printf("\n");

		for (int i = 0 ; i < SIZE ; i++)
			printf("[%d] ", buffer[i]);

		printf("\nBuffer %s\n", buffer);
		printf("\nRETURN %u\n", ft_strlcpy(buffer, argv[1], SIZE - 1));



		printf("\n NORMAL STRLCPY \n");

		for (int i = 0 ; i < SIZE ; i++)
			printf("[%d] ", buffer2[i]);

		printf("\nRETURN %lu\n", strlcpy(buffer2, argv[1], SIZE - 1));
		printf("\n");

		for (int i = 0 ; i < SIZE ; i++)
			printf("[%d] ", buffer2[i]);
		printf("\nBuffer %s\n", buffer2);

	}
	return (0);
}
